import React from 'react';
//Routing
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Startscherm from './components/Homepage/StartIndex';
import IntroScherm from './components/introductie/IntroIndex'
import MainInterface from './components/Maininterface/Maininterface';
import Tuinderinfo from "./components/TuinderInfo/TuinderInfoHTML";
import Tuinderinterface from './components/Tuinderinterface/Tuinderinterface';
import Maps from './components/Maps/MapsIndex';
import NotFound from './components/Routing/NotFound';
import {GlobalStyle} from './GlobalStyles';
import Oldmaps from './components/Maps/Oldmaps';
import Currentmap from './components/Maps/Currentmap';

function App() {

  return(
    <Router>
      <Routes>
        <Route path='/' element={<Startscherm/>} />
        <Route path='/intro' element={<IntroScherm/>} />
        <Route path='/main' element={<MainInterface/>}/>
        <Route path='/tuinderinterface' element={<Tuinderinterface/>} />
        <Route path='/tuinderinfo' element={<Tuinderinfo/>} />
        <Route path='/map' element={<Maps/>} />
        <Route path='/oldmaps' element={<Oldmaps/>} />
        <Route path='/currentmap' element={<Currentmap/>} />
        <Route path='/*' element={<NotFound/>} />
      </Routes>
      <GlobalStyle />
    </Router>
  );
}


export default App;

