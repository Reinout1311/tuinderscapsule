import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
:root {
    --maxwidth: 1920px;
}

* {
    box-sizing: border-box;
}

body {
    margin: 0;
    padding: 0;
}
a:link{
    text-decoration: none;
    font-family: 'Calibri',Helvetica,Arial,Lucida,sans-serif;
    color: #288887;
}
p {
    color: black;
}
`

