import React from "react";
import "../Maininterface/Maininterface.css";
import { Link } from "react-router-dom";

const Main = () => {
    return(
    <div id='Menu'>
      <div id="Top"/>
    <div id="menuArea">
      <div id="menuContent">
        <Link to="/tuinderinterface"className=" Options" id="Search-Tuinder">
          <p id="Tuinders">Tuinders</p>
          <img id="BGtuinder"></img>
        </Link>
        <Link to="/map" className=" Options" id="Search-Adress">
          <p id="Adres">Adres zoeken</p>
          <img id="BGadres"></img>
        </Link>
      </div>
    </div>
    <div id="Bottom"></div>
  </div>
  ) 
}
    
  export default Main