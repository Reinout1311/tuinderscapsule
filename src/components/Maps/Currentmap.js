import React from 'react';
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet'
import '../Maps/Maps.css';
import  {Bottom} from '../Homepage/Start.styles.js';
import Tuinders from '../../tuinders.json';
import person from '../../images/person.png';
import { Link } from 'react-router-dom';

const startPosition = [52.097806, 5.033455]
const tuinders = Tuinders.tuinders;

function Currentmap() {
  return (
    <div id='Basic'>
    <div id='Top'>
    </div>
    <MapContainer
        className="map"
        center={startPosition}
        zoom={14}
        zoomControl={false}
        bounceAtZoomLimits={true}
        maxBounds={[
          [52.063045, 4.972515],
          [52.129905, 5.098343],
        ]}>
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={[52.106292542693296, 5.011721767999341]}>
        <Popup>
        Historisch vereniging Vleuten <br/> Broederschapshuisjes 
        </Popup>
      </Marker>
      //{
        tuinders && tuinders.map( tuinder => {
          return(
            <div>
              <Marker position={[tuinder.latitude, tuinder.longitude]}>
                <Popup>
                  <img id="placeholder" src={person}></img>
                  <a href='https://google.com'>{tuinder.firstName}</a>
                </Popup>
              </Marker>
            </div>
          )
        }
        )
      }
    </MapContainer>
    <Bottom/>
    </div>
  )
}
export default Currentmap;