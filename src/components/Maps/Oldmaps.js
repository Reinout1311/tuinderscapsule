import React, { useEffect, useState } from "react";
import L from "leaflet";
import { ImageOverlay, MapContainer, Marker, Popup, TileLayer, useMapElement} from 'react-leaflet'
import '../Maps/Maps.css';
import  {Bottom} from '../Homepage/Start.styles.js';
import Tuinders from '../../tuinders.json';
import person from '../../images/person.png';
import { Link } from 'react-router-dom';
import { LatLngBounds } from "leaflet";
import oldMap1959 from "../../images/1959_Stitched.png"
import oldMap1948 from "../../images/1948_Stitched.png"
import oldMap1970 from "../../images/1970_Stitched.png"
import oldMap1981 from "../../images/1981_Stitched.png"
import oldMap1992 from "../../images/1992_Stitched.png"
import oldMap2001 from "../../images/2001_Stitched.png"
import oldMap2010 from "../../images/2010_Stitched.png"
import oldMap2019 from "../../images/2019_Stitched.png"


const startPosition = [52.097806, 5.033455]
const bounds = new LatLngBounds([52.15011467869643, 4.8771041164791855], [52.03776590818209, 5.1680376354643265],)
const tuinders = Tuinders.tuinders;



const Oldmaps= () => {

  const [timeBracket, newTimeBracket] = useState(1)
  const [mapChoice, newMapChoice] = useState(oldMap1948)

  // useEffect= () => {
  // //   console.log(document.getElementsByClassName("map"))

  // //   var mapOptions = {
  // //     center: [17.342761, 78.552432],
  // //     zoom: 8
  // //  }

  // //  var map = new L.map('map', mapOptions); // Creating a map object
   
  // //  // Creating a Layer object
  // //  var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  // //  map.addLayer(layer);  // Adding layer to the map
   
  // //  // Creating Image overlay
  // //  var imageUrl = "src/images/1948.jpg"
  // //  var imageBounds = [[17.342761, 78.552432], [16.396553, 80.727725]];
  // //  var overlay = L.imageOverlay(imageUrl, imageBounds);
  // //  overlay.addTo(map);

  //   }

  return (
    <div id='Basic'>
    <div id='Top'>
      <Link to='/main'>
      <div id='BackButton'>
        <div id='Arrowcontainer'>
        <div id='Arrow'></div>
        <div id='Arrowtail'></div>
        <p id='back'>Terug</p>
        </div>
      </div>
      </Link>
      <input type='radio' id='timebracket1'className="TimeBracket" name="TimeBracket" onClick= { () => {newTimeBracket(1)}} onChange= { () => {newMapChoice(oldMap1948)}}></input>
      <input type='radio' id='timebracket2' className="TimeBracket" name="TimeBracket" onClick= { () => {newTimeBracket(2)}} onChange= { () => {newMapChoice(oldMap1959)}}></input>
      <input type='radio' id='timebracket3' className="TimeBracket" name="TimeBracket" onClick= { () => {newTimeBracket(3)}} onChange= { () => {newMapChoice(oldMap1970)}}></input>
      <input type='radio' id='timebracket4' className="TimeBracket" name="TimeBracket" onClick= { () => {newTimeBracket(4)}} onChange= { () => {newMapChoice(oldMap1981)}}></input>
      <input type='radio' id='timebracket5' className="TimeBracket" name="TimeBracket" onClick= { () => {newTimeBracket(5)}} onChange= { () => {newMapChoice(oldMap1992)}}></input>
      <input type='radio' id='timebracket6' className="TimeBracket" name="TimeBracket" onClick= { () => {newTimeBracket(6)}} onChange= { () => {newMapChoice(oldMap2001)}}></input>
      <input type='radio' id='timebracket7' className="TimeBracket" name="TimeBracket" onClick= { () => {newTimeBracket(7)}} onChange= { () => {newMapChoice(oldMap2010)}}></input>
      <input type='radio' id='timebracket8' className="TimeBracket" name="TimeBracket" onClick= { () => {newTimeBracket(8)}} onChange= { () => {newMapChoice(oldMap2019)}}></input>
      <div id='years'>
      <label for='timebracket1' id='year1' className='years'>1900-1919</label>
      <label for='timebracket2' id='year2' className='years'>1920-1939</label>
      <label for='timebracket3' id='year3' className='years'>1940-1959</label>
      <label for='timebracket4' id='year4' className='years'>1960-1979</label>
      <label for='timebracket5' id='year5' className='years'>1980-1989</label>
      <label for='timebracket6' id='year6' className='years'>1990-1999</label>
      <label for='timebracket7' id='year7' className='years'>2000-2009</label>
      <label for='timebracket8' id='year8' className='years'>2010-NU</label>
      </div>
    </div>
    <MapContainer
        className="map"
        center={startPosition}
        zoom={17}
        zoomControl={true}
        minZoom={14}
        maxZoom={16}
        bounceAtZoomLimits={true}
        maxBounds={[
          [52.063045, 4.972515],
          [52.129905, 5.098343],
        ]}>
          <ImageOverlay
          url={mapChoice}
          bounds={bounds}
          ></ImageOverlay>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={[52.106292542693296, 5.011721767999341]}>
        <Popup>
        Historisch vereniging Vleuten <br/> Broederschapshuisjes 
        </Popup>
      </Marker>
      //{
        tuinders.filter(tuinder => tuinder.timebracket === timeBracket).map( tuinder => {
          return(
            <div>
              <Marker position={[tuinder.latitude, tuinder.longitude]}>
                <Popup>
                  <img id="placeholder" src={person}></img>
                  <a href='https://google.com'>{tuinder.firstName}</a>
                </Popup>
              </Marker>
            </div>
          )
        }
        )
      }
    </MapContainer>
    <Bottom/>
    </div>
  )
}
export default Oldmaps;













