import React, { useState } from "react";
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet'
import '../Maps/Maps.css';
import  {Bottom} from '../Homepage/Start.styles.js';
import Tuinders from '../../tuinders.json';
import person from '../../images/person.png';
import SearchIcon from '../../images/MagnifyingGlass.png';
import { Link } from 'react-router-dom';

const startPosition = [52.097806, 5.033455]
const tuinders = Tuinders.tuinders;


function Map() {

const [timeBracket, newTimeBracket] = useState(1)

function updateTimeBracket(newTime){
        
  localStorage.setItem("timeBracket", newTime)
  newTimeBracket(parseInt(localStorage.getItem("timeBracket")))
  console.log(localStorage.getItem("timeBracket"))
}

  return (
    <div id='Basic'>
    <div id='Top'>
      <Link to='/main'>
      <div id='BackButton'>
        <div id='Arrowcontainer'>
        <div id='Arrow'></div>
        <div id='Arrowtail'></div>
        <p id='back'>Terug</p>
        </div>
      </div>
      </Link>
      <input type='radio' id='timebracket1'className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(1)}}></input>
      <input type='radio' id='timebracket2' className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(2)}}></input>
      <input type='radio' id='timebracket3' className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(3)}}></input>
      <input type='radio' id='timebracket4' className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(4)}}></input>
      <input type='radio' id='timebracket5' className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(5)}}></input>
      <input type='radio' id='timebracket6' className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(6)}}></input>
      <input type='radio' id='timebracket7' className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(7)}}></input>
      <input type='radio' id='timebracket8' className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(8)}}></input>
      <div id='years'>
      <label for='timebracket1' id='year1' className='years'>1900-1919</label>
      <label for='timebracket2' id='year2' className='years'>1920-1939</label>
      <label for='timebracket3' id='year3' className='years'>1940-1959</label>
      <label for='timebracket4' id='year4' className='years'>1960-1979</label>
      <label for='timebracket5' id='year5' className='years'>1980-1989</label>
      <label for='timebracket6' id='year6' className='years'>1990-1999</label>
      <label for='timebracket7' id='year7' className='years'>2000-2009</label>
      <label for='timebracket8' id='year8' className='years'>2010-NU</label>
      </div>
    </div>
    <div id='Searchbar'>
      <img id='SearchIcon'src={SearchIcon}></img>
      <input type='text' id='adresinput' placeholder='Vul het adres hier in...' maxLength={45}></input>
    </div>
    <MapContainer
        className="map"
        center={startPosition}
        zoom={17}
        zoomControl={true}
        minZoom={13}
        maxZoom={16}
        bounceAtZoomLimits={true}
        maxBounds={[
          [52.063045, 4.972515],
          [52.129905, 5.098343],
        ]}>
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={[52.106292542693296, 5.011721767999341]}>
        <Popup>
        Historisch vereniging Vleuten <br/> Broederschapshuisjes 
        </Popup>
      </Marker>
      //{
        tuinders.filter(tuinder => tuinder.timebracket === timeBracket).map( tuinder => {
          return(
            <div>
              <Marker position={[tuinder.latitude, tuinder.longitude]}>
                <Popup>
                  <img id="placeholder" src={person}></img>
                  <a href='https://google.com'>{tuinder.firstName}</a>
                </Popup>
              </Marker>
            </div>
          )
        }
        )
      }
    </MapContainer>
    <Bottom/>
    </div>
  )
}
export default Map;