import "./Tuinderinterface.css";
import { Link } from "react-router-dom";
import Tuinders from '../../tuinders.json'
import React, {useState} from 'react'
import "./Tuinder.css"
import Person from '../../images/person.png'

const Tuinderinterface = () => {

    const [timeBracket, newTimeBracket] = useState(1)
    // const [tuinderPage, newTuinderPage] = useState()
    const tuinderList = Tuinders.tuinders;


    function updateTimeBracket(newTime){
        
        localStorage.setItem("timeBracket", newTime)
        newTimeBracket(parseInt(localStorage.getItem("timeBracket")))
        console.log(localStorage.getItem("timeBracket"))
    }


    function filterTuinderList(tuinderList) {

        let filteredTuinderList = tuinderList.filter(tuinder => tuinder.timebracket === timeBracket);
        if (filteredTuinderList.length > 3) {
            filteredTuinderList.splice(3, filteredTuinderList.length)
        }
        return filteredTuinderList;

    }

    // TODO Ervoor zorgen dat geen div gemaakt wordt als de gefilterde lijst te kort is (en dus bijv. 'tuinder3' niet bestaat).
    // TODO Een manier vinden om de te weergeven tuinders binnen een tijdbracket te veranderen met de arrow buttons.

 return(
    <div id="Basic">
        <div id="Top">
            <Link to='/main'>
                <div id='BackButton'>
                    <div id='Arrowcontainer'>
                        <div id='Arrow'/>
                <div id='Arrowtail'/>
            <p id='back'>Terug</p>
        </div>
      </div>
      </Link>
            <input type='radio' id='timebracket1'className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(1)}}/>
            <input type='radio' id='timebracket2' className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(2)}}/>
            <input type='radio' id='timebracket3' className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(3)}}/>
            <input type='radio' id='timebracket4' className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(4)}}/>
            <input type='radio' id='timebracket5' className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(5)}}/>
            <input type='radio' id='timebracket6' className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(6)}}/>
            <input type='radio' id='timebracket7' className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(7)}}/>
            <input type='radio' id='timebracket8' className="TimeBracket" name="TimeBracket" onClick={() => {updateTimeBracket(8)}}/>
            <div id='tuinderyears'>
            <label for='timebracket1' id='tuinderyear1' className='years'>1900-1919</label>
            <label for='timebracket2' id='tuinderyear2' className='years'>1920-1939</label>
            <label for='timebracket3' id='tuinderyear3' className='years'>1940-1959</label>
            <label for='timebracket4' id='tuinderyear4' className='years'>1960-1979</label>
            <label for='timebracket5' id='tuinderyear5' className='years'>1980-1989</label>
            <label for='timebracket6' id='tuinderyear6' className='years'>1990-1999</label>
            <label for='timebracket7' id='tuinderyear7' className='years'>2000-2009</label>
            <label for='timebracket8' id='tuinderyear8' className='years'>2010-NU</label>
            </div>
        </div>
        <div id="ArrowButtonL" >
            <div id="ArrowL"/>
        </div>
        <div id="ArrowButtonR">
            <div id="ArrowR"/>
        </div>
        <div id='Tuindercontainer'> 
            <div className='TuinderBox'>

                {
                   filterTuinderList(tuinderList).map(tuinder => {
                       return (
                           <div className = "Tuinder" key={tuinder.id}>
                               <strong>{tuinder.firstName}</strong><br />
                               <img src={Person} alt = "Tuinder" width = "200" height = "200"/>
                               {tuinder.description}<br /><br />
                           </div>
                       )
                   })
                }

            </div>
    </div>
    <div id="Bottom"/>
    </div>
    )
}

export default Tuinderinterface