import styled from "styled-components";


export const HomePage = styled.div`
height: 100vh;
width: 100vw;
`
export const Top = styled.div`
background-color: #008A8B;
height: 6vh;
z-index: 900;
`;

export const Mid = styled.div`
height: 88vh;
display: flex;
flex-direction: column;
align-items: center;
`;

export const FlagNL = styled.img`
width: 45px;
height: 45px;
margin-top: 5px;
margin-left: 20px;
margin-right: 20px;
border-radius: 70%;
border-style: outset;
border-width: 3px
`
export const FlagGB = styled.img`
width: 45px;
height: 45px;
border-radius: 50%;
border-style: outset;
border-width: 3px
`

export const LogoImg = styled.img`
width: 800px;
height: 500px;
margin: 0 auto;
margin-top: 50px;
`;

export const HomeButton = styled.div`
width: 100vw;
height: 200px;
margin: 0 auto;
margin-top: 100px;
`;

export const Bottom = styled.div`
background-color: #008A8B;
height: 6vh;
`;
