import React, {useState, useEffect} from 'react';
import '../Homepage/Fader.css'
import PropTypes from 'prop-types'

const Fader = ({ text }) => {

    const [fadeProp, setFadeProp] = useState({
        fade: 'fade-in',
    });

    useEffect(() =>{
        const timeout = setInterval(() => {
            if(fadeProp.fade === 'fade-in'){
                setFadeProp({
                    fade: 'fade-out'
                })
            } else {
                setFadeProp({
                    fade: 'fade-in'
                })
            }
        }, 1500);

        return () => clearInterval(timeout)
    }, [fadeProp])
    return(
        <>
            <h1 className={fadeProp.fade}>{text}</h1>
        </>
    )
}

Fader.defaultProps = {
    text: 'Raak het scherm aan om door te gaan'
}

Fader.propTypes = {
    text: PropTypes.string,
}

export default Fader