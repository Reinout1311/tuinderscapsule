import React from "react";
import {Link} from 'react-router-dom';
import LogoHV from "../../images/logo_HV.png";
import Fader from './Fader.js';
import VlagNL from '../../images/Nederland.png';
import VlagGB from '../../images/Britain.png';
import Tuinders from '../../tuinders.json';


import{HomePage, Top, FlagNL, FlagGB, Mid, LogoImg, HomeButton, Bottom} from './Start.styles';

const Startscherm = () => (
    <HomePage>
        <Top>
            <FlagNL src={VlagNL}/>
            <FlagGB src={VlagGB}/>   
        </Top>
        <Link to='/Intro' style={{color: 'black'}}>
        <Mid>
        <LogoImg src={LogoHV}/>
            <HomeButton>
                <Fader></Fader>
            </HomeButton>
        </Mid>
        </Link>
        <Bottom></Bottom>
    </HomePage>
);

export default Startscherm;