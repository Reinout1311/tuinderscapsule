import React from 'react';
import '../TuinderInfo/TuinderInfo.css';
import tuinderIcon from '../../images/tuinderIcon.png';
import background from '../../images/placeHolderImageTuinder.jpg';


function Tuinder() {
  return (
    <div id='basicDiv'> 
        <div id='topDiv'>
            <div id='background'>
                <img id='Background' src={background}/>
            </div>
        </div>
        <div id='bottomDiv'>
            <div id='textDiv'>
                <h1>Tuinder bio</h1>
                <h3>Tuinder naam</h3>
        
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                Viverra adipiscing at in tellus. Sed sed risus pretium quam vulputate. <br/> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                Viverra adipiscing at in tellus. Sed sed risus pretium quam vulputate. 
                </p>
                
            </div>
            <div id='secondTextDiv'>
                <h2>Tuinder infomatie</h2>
        
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                Viverra adipiscing at in tellus. Sed sed risus pretium quam vulputate.  <br/> Viverra adipiscing at in tellus. Sed sed risus pretium quam vulputate. 
                <br/> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                Viverra adipiscing at in tellus. Sed sed risus pretium quam vulputate. 
                </p>
            </div>
            <div id='slideShowDiv'>
                <img id='TuinderIcon' src={tuinderIcon}/>
            </div>
        </div>
    </div>
  );
}

export default Tuinder;