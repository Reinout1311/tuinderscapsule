import styled from "styled-components";
import backGround from "../../images/Background.jpg";

export const MainPage = styled.div`
height: 100vh;
width: 100vw;
`;

export const Top = styled.div`
background-color: #008A8B;
height: 6vh;
`;

export const Mid = styled.div`
height: 88vh;
display: flex;
flex-direction: column;
align-items: center;
position: relative;
background: url(${backGround});
background-size: cover
`;

export const Vid = styled.video`
height: 700px;
width: 1200px;
margin-top: auto;
margin-bottom: auto;
border-style: inset;
border-width: 10px;
border-color: #008A8B;
object-fit: fill;
`;

export const IntroButton = styled.div`
background-color: #0a6bff;
width: 300px;
height: 70px;
margin: 0 auto;
margin-bottom: 30px;
text-align: center;
font-size: 50px;
`;

export const Bottom = styled.div`
background-color: #008A8B;
height: 6vh;
`;