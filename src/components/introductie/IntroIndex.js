import React from "react";
import {Link} from 'react-router-dom';

import{MainPage, Top, Mid, Vid, IntroButton, Bottom} from './Intro.styles';
import Doggo from '../../Video/Puppy.mp4';
 

const IntroScherm = () => (
    <MainPage>
        <Top/>
            <Mid>
                <Vid controls src={Doggo}/>
                <Link to='/main'>
                    <IntroButton>Overslaan</IntroButton>
                </Link>
            </Mid>
        <Bottom></Bottom>
    </MainPage>
);

export default IntroScherm